# How to setup an Angular pipeline with GitLab

I do't know if you are aware but GitLab is a great git tool. It enables you to create private repository that you can deploy on GitLab Pages with GitLab CI/CD. In this article, we will setup a gitlab pipeline that will :
* run the lint
* run unit tests
* run unit e2e
* build the project in production mode
* deploy the project to a GitLab Pages.

First let's create a simple project using Angular CLI:
```
ng new angular-gitlab-ci
```

Now we can setup the GitLab pipeline by creating a *.gitlab-ci.yml* :
```
image: node:latest

stages:
  - build
  - test
  - deploy

cache:
    paths:
    - ./node_modules

build:
  script:
    - npm install
    - npm run build:gitlab
  artifacts:
    paths:
      - dist/

test:
  script:
    - npm install
    - npm run lint
    - npm run test
    - npm run e2e

pages:
  stage: deploy
  script:
  - mv dist public
  artifacts:
    paths:
    - public
  only:
  - master
```

You will also need to update your *package.json* with the following :
```
"scripts": {
  ...
  "build:gitlab": "ng build --prod --base-href /angular-gitlab-ci/",
  "test:chromeHeadless": "npm run test -- --browsers ChromeHeadless --single-run",
}
```

*angular-gitlab-ci* should be the same name as your repository on GitLab.
```
module.exports = function (config) {
  config.set({
    ...
    customLaunchers: {
      ChromeHeadless: {
        base: 'Chrome',
        flags: [
          '--headless',
          '--disable-gpu',
          // Without a remote debugging port, Google Chrome exits immediately.
          '--remote-debugging-port=9222',
        ],
      }
    }
```

##Issue with assets directly in index.html
All your assets are now behind /angular-gitlab-ci/assets/* when you are deployed, so the browser will not find your favicon if you set it up like that in the *index.html*:
```
  <link rel="icon" type="image/x-icon" href="/assets/images/icons/favicon-16x16.png">
```

One solution is to add into your *app.component.ts* :

```
 constructor(@Inject('Document') private document: Document) {
    // Assets path upading according to base href
    let baseHref = document.getElementsByTagName('base')[0].attributes.getNamedItem('href').value;
    baseHref = baseHref.substring(0, baseHref.length - 1);

    for (const el of Array.from(document.getElementsByTagName('link'))) {
      const href = el.attributes.getNamedItem('href') ? el.attributes.getNamedItem('href').value : '';
      if (href.match(/^\//)) {
        el.attributes.getNamedItem('href').value = baseHref + href;
      }
    }

    for (const el of Array.from(document.getElementsByTagName('meta'))) {
      const href = el.attributes.getNamedItem('content') ? el.attributes.getNamedItem('content').value : '';
      if (href.match(/^\//)) {
        el.attributes.getNamedItem('content').value = baseHref + href;
      }
    }
  }
```
It will append the baseHref you set up on link href and meta content if needed. 

Note, do not forgot to provide *Document* in your *app.module.ts*:
```
@NgModule({
  ...
  providers: [
    { provide: 'Document', useValue: document }
  ]
  ...
})
export class AppModule { }
```

##Issue with @ngx-translate
For the same reason, you could aslo not be able to load your translation if you use @ngx-transalate and you store them at */assets/i18n*. To fix it, you need to set up the TranslateModule as for AOT which means :
```
...
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
...

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/');
}

@NgModule({
  ...
  imports: [
    ...
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  ...
})
export class AppModule { }

```